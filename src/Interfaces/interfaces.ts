export interface iOption {
    label:string;
    value:string;
}

export interface iQuestion {
    questionId: string;
    question: string;
    isRequired?: boolean;
    questionType: string;
    options?: iOption[];
    allowMoreThanOneOption?: boolean;
    inEditMode: boolean;
    isMultiline?: boolean;
}