import * as React from 'react';
import SURVEY from '../../Store/constants';
import * as Sortable from 'sortablejs';
import * as ReactDOM from 'react-dom';


export default class QuestionTypes extends React.Component<{}, {questionTypes:{ name:string; value:string; iconClass:string; }[], selectedType:string}> {
    sortable:any = null;
    constructor(props:any){
        super(props);
        this.state = {
            questionTypes: SURVEY.INITIAL_STORE.questionTypes,
            selectedType: ''
        };
    }
    componentDidMount(){
        var qtSortable = Sortable.create(document.getElementById('questionTypes'), {
            animation: 200,
            ghostClass: 'ghost',
            group: {
              name: "shared",
              pull: "clone",
              put: false
            },
            sort: false
        });
    }
    render(){
        const items = this.state.questionTypes.map((questionType:any, index:number)=>{
            return(
                <div data-id={questionType.value} className="questionType" key={index}>
                    <i className={questionType.iconClass}></i>
                    {questionType.name}
                </div>
            )
        });
        return(
            <div className="d-flex flex-wrap" id="questionTypes">                
                { 
                    items
                }
            </div>
        )
    }
}