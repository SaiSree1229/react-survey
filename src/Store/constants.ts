const SURVEY = {
    ADD_QUESTION: 'ADD_QUESTION',
    INITIAL_STORE: {
        questions: [{}],
        questionTypes: [
            {
                name: 'Multiple Choice',
                value: 'MC',
                iconClass: 'fa fa-check-circle-o',
            },
            {
                name: 'Dropdown',
                value: 'DD',
                iconClass: 'fa fa-arrow-circle-down',
            },
            {
                name: 'Yes / No',
                value: 'YN',
                iconClass: 'fa fa-adjust',
            },
            {
                name: 'Text Field',
                value: 'TF',
                iconClass: 'fa fa-keyboard-o',
            },
            {
                name: 'Heading',
                value: 'HE',
                iconClass: 'fa fa-header',
            }
        ]
    }
}

export default SURVEY;