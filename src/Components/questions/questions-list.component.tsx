import * as React from 'react';
import Question from './question.component';
import * as Sortable from 'sortablejs';
import * as idGenerator from 'uuid/v4';
import * as ReactDOM from 'react-dom';
import { iQuestion } from '../../Interfaces/interfaces';
import * as _ from 'lodash';


export default class QuestionList extends React.Component<{}, { selectedType:string; questions:iQuestion[] }> {

    constructor(props:any) {
        super(props);
        this.state = {
            selectedType: '',
            questions: []
        }
    }
    componentDidMount(){
        let self = this;
        let qlSortable = Sortable.create(document.getElementById('questionsList'), {
            group: {
                name: "shared",
                pull: false,
                put: true
            },
            sort: true,
            onAdd: function (evt:any) {         
                self.addQuestion(evt.item.getAttribute('data-id'));
                evt.item.parentNode.removeChild(evt.item);                
            },
            onSort: function (evt:any) {
                console.log(self.state);
                console.log(qlSortable.toArray());
            }
        });
    }

    addQuestion = (type:string) => {

        let questionObj:iQuestion = {
            questionId: idGenerator(),
            questionType: type,
            question: 'What question would you like to ask?',
            isRequired: false,
            inEditMode: true,
            isMultiline: false,
            allowMoreThanOneOption: false,
            options: []
        };

        this.setState({
            selectedType: type,
            questions: [
                ...this.state.questions,
                questionObj
            ]
        });
    }

    saveQuestion = (objQuestion:iQuestion) => {
        console.log(objQuestion);
        let i = _.findIndex(this.state.questions, function(o) { return o.questionId == objQuestion.questionId; });
        this.setState((prevState)=> {
            prevState.questions[i] = objQuestion
        });
    }

    deleteQuestion = (questionId:string) => {
        let i = _.findIndex(this.state.questions, function(o) { return o.questionId == questionId; });
        let ql = this.state.questions.slice(i, 1);
        this.setState({ selectedType: '', questions: ql  });
    }


    render(){
        const questions = this.state.questions.map((question:iQuestion, index:number) => {
            let props = {
                questionData: question,
                key: question.questionId,
                deleteQuestion: this.deleteQuestion,
                saveQuestion: this.saveQuestion
            };
            return <Question {...props} />            
        });
        return(
            <div id="questionsList">
                { questions }
            </div>
        )
    }
}