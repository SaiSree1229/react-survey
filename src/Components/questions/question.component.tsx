import * as React from 'react';
import { iQuestion, iOption } from '../../Interfaces/interfaces';
import * as Sortable from 'sortablejs';

interface iQuestionProps {
    questionData: iQuestion,
    deleteQuestion: (questionId: string) => void;
    saveQuestion: (question:iQuestion) => void;
}

interface iQuestionState extends iQuestion {
}

export default class Question extends React.Component<iQuestionProps, iQuestionState> {
    constructor(props:any) {
        super(props);
        this.state = {
            inEditMode: this.props.questionData.inEditMode,
            questionId: this.props.questionData.questionId,
            question: this.props.questionData.question,
            questionType: this.props.questionData.questionType,
            allowMoreThanOneOption: false,
            isRequired: false,
            isMultiline: this.props.questionData.isMultiline,
            options: [
                {
                    label: 'Option1',
                    value: ''
                },
                {
                    label: 'Option2',
                    value: ''
                },
                {
                    label: 'Option3',
                    value: ''
                }
            ]
        }
        this.optionsId = "options-" + this.state.questionId;
    }

    optionsId:string;

    saveQuestion = () => {
        this.setState({inEditMode : false});
        this.props.saveQuestion(this.state);
    }

    deleteQuestion = () => {
        this.props.deleteQuestion(this.props.questionData.questionId)
    }

    cardBody = ()=>{        
        switch(this.state.questionType){
            case 'HE': {
                if(this.state.inEditMode) {
                    return (
                        <div>
                            <h5 className="d-flex justify-content-between">
                                Edit Heading
                                <span className="btn-group">
                                    <i className="btn btn-sm btn-light fa fa-save" onClick={() => { this.saveQuestion() }}></i>
                                    <i className="btn btn-sm btn-light fa fa-trash" onClick={()=>{ this.deleteQuestion() }}></i>
                                </span>
                            </h5>
                            <input className="form-control rounded-0" type="text" 
                            onChange={(e)=>{ this.setState({ question: e.target.value }) }} 
                            defaultValue={this.state.question} />
                        </div>
                    )
                } else {
                    return (
                        <h5>{ this.state.question }</h5>
                    )
                }
            }
            case 'MC': {
                if(this.state.inEditMode) {
                    return(
                        <div>
                            <h5 className="d-flex justify-content-between">
                                Multiple Choice
                                <span className="btn-group">
                                    <i className="btn btn-sm btn-light fa fa-save" onClick={() => { this.saveQuestion() }}></i>
                                    <i className="btn btn-sm btn-light fa fa-trash" onClick={()=>{ this.deleteQuestion() }}></i>
                                </span>
                            </h5>
                            <small>Question</small>
                            <input className="form-control rounded-0" type="text"
                            onChange={(e) => { this.setState({ question: e.target.value }) }} 
                            defaultValue={this.state.question} />
                            <h5 className="mt-4">Choices</h5>
                            <p></p>
                            <div id={this.optionsId} className="options">
                            {
                                this.state.options.map((option: iOption, index: number) => {
                                    return(
                                        <div className="d-flex align-items-center option" key={index}>
                                            <i className="fa fa-arrows mr-2"></i>
                                            <input className="form-control rounded-0" type="text"
                                                defaultValue={option.label} onBlur={(e)=>{ option.label = e.target.value; }} />
                                        </div>
                                    )
                                })
                            }
                            </div>
                            <label className="mt-3">
                                <input type="checkbox" className="mr-1" checked={this.state.allowMoreThanOneOption} 
                                onChange={(e)=>{ let ischecked = e.target.checked;this.setState({ allowMoreThanOneOption: ischecked }); }}/>
                                Allow more thn one answer to this question(Use checkboxes)
                            </label>
                        </div>
                    )
                } else {
                    return (
                        <div>
                            <h5>{ this.state.question }</h5>
                            {
                                this.state.options.map((option: iOption, index: number) => {
                                    if(this.state.allowMoreThanOneOption) {
                                        return(
                                            <div key={index}>
                                                <label>
                                                    <input className="mr-1" type="checkbox" defaultValue={option.value} name={this.state.questionId}/>
                                                    {option.label}
                                                </label>
                                            </div>                                    
                                        )                                        
                                    } else {
                                        return(
                                            <div key={index}>
                                                <label>
                                                    <input className="mr-1" type="radio" defaultValue={option.value} name={this.state.questionId}/>
                                                    {option.label}
                                                </label>
                                            </div>                                    
                                        )
                                    }                                
                                })
                            }
                        </div>
                    );
                }
            }
            case 'TF': {
                if(this.state.inEditMode) {
                    return(
                        <div>
                            <h5 className="d-flex justify-content-between">
                                Text Field
                                <span className="btn-group">
                                    <i className="btn btn-sm btn-light fa fa-save" onClick={() => { this.saveQuestion() }}></i>
                                    <i className="btn btn-sm btn-light fa fa-trash" onClick={()=>{ this.deleteQuestion() }}></i>
                                </span>
                            </h5>
                            <small>Question</small>
                            <input className="form-control rounded-0" type="text" 
                            onChange={(e)=>{ this.setState({ question: e.target.value }) }} 
                            defaultValue={this.state.question} />
                            <div className="mt-2">
                                <small>Answer Type</small>
                            </div>
                            <label className="mr-2">
                                <input className="mr-1" type="radio" value="false" checked={this.state.isMultiline == false} onChange={(e)=>{ this.setState({ isMultiline: (e.target.value === 'true') }); }}/> Single line
                            </label>
                            <label>
                                <input className="mr-1" type="radio" value="true" checked={this.state.isMultiline == true} onChange={(e)=>{ this.setState({ isMultiline: (e.target.value === 'true') }) }}/> Multiple lines
                            </label>
                        </div>
                    );
                } else {
                    let input = null;
                    if(!this.state.isMultiline) {
                        input = <input className="form-control rounded-0"/>
                    } else {
                        input = <textarea className="form-control rounded-0"/>
                    }
                    return(
                        <div>
                            <h5>{ this.state.question }</h5>
                            { input }
                        </div>
                    );
                }
            }
        }
    }

    componentDidMount(){
        if( ['HE', 'TF'].indexOf(this.state.questionType) == -1 ) {
            let oid = this.optionsId;
            Sortable.create(document.getElementById(oid),{
                group: {
                    name: oid,
                    pull: false,
                    put: false
                },
                sort: true
            });
        }
    }

    render(){
        return(

            <div className="card rounded-0" data-id={this.state.questionId}>
                <div className="card-body">                    
                    {
                        this.cardBody()
                    }
                </div>
            </div>
        )
    }
}