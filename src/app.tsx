import * as React from 'react';
import {render} from 'react-dom';
import QuestionsList from './Components/questions/questions-list.component';
import QuestionTypes from './Components/questionTypes/question-types.component';

class App extends React.Component {
    
    render(){
        return(
            <div className="row">
                <div className="col-sm-12 col-md-8">
                    <QuestionsList/>
                </div>
                <div className="col-sm-12 col-md-4">
                    <QuestionTypes/>
                </div>            
            </div>
        )
    }
} 

render(<App/>, document.getElementById('root'));